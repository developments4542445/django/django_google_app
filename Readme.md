# Django Google App

Project where we deploy a serverless Django app on Google App Engine.

This project is based in:
- Python 3.10.9
- Django
- Docker

Libraries and others:
- Django==3.2.5

Steps procedure:
- Create Project
- Deploy to Google App Engine
- Setup Datastore
- Setup Secret Manager
- Build Bouncer

Commands used in this project (executed from powershell terminal):
- Create a new project
  - docker-compose run --rm app sh -c "django-admin startproject app ."
- Run server
  - docker-compose up
- Create an app
  - docker-compose run --rm app sh -c "python manage.py startapp bouncer"
- Checking that the docker-compose-deploy.yml file is correct
  - docker-compose -f docker-compose-deploy.yml run --rm gcloud gcloud --version
- Google App Engine webpage: https://console.cloud.google.com/
  - https://console.cloud.google.com/appengine/services?serviceId=default&project=bouncer-demo-409218
- Run test in docker-compose:
  - docker-compose run --rm app sh -c "python manage.py test"
- Execute commands in 'commands.txt' via django shell:
  - docker-compose run --rm app sh -c "python manage.py shell"
- Run tests for secrets.py:
  - docker-compose run --rm app sh -c "python manage.py test"
- Deploy and update latest files to Google Cloud Storage:
  - docker-compose -f docker-compose-deploy.yml run --rm gcloud gcloud app deploy --project bouncer-demo-409218
- Collect static command:
  - docker-compose run --rm app sh -c "python manage.py collectstatic"
- Deploy and update latest files to Google Cloud Storage:
  - docker-compose -f docker-compose-deploy.yml run --rm gcloud gcloud app deploy --project bouncer-demo-409218
 